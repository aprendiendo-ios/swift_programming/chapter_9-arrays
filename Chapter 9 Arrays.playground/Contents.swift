import Cocoa

var bucketList = ["Climb Mt. Everest"]

var newItems = ["Fly hot air ballon to Fiji",
                "Watch the Lord of the Rings trilogy in one day",
                "Go on a walkabout",
                "Scuba dive in the Great Blue Hole",
                "Find a triple rainbown"]
bucketList += newItems
bucketList
bucketList.remove(at: 2)
print(bucketList.count)
print(bucketList[0...2])
bucketList[2] += " in Australia"
bucketList[0] = "Climb Mt. Kilimanjaro"
bucketList.insert("Toboggan across Alaska", at: 2)
bucketList

var myronsList = [
                "Climb Mt. Kilimanjaro",
                "Fly hot air ballon to Fiji",
                "Toboggan across Alaska",
                "Go on a walkabout in Australia",
                "Scuba dive in the Great Blue Hole",
                "Find a triple rainbown"]

let equal = (bucketList == myronsList)

let lunches = ["Cheeseburger",
               "Veggie Pizza",
               "Chicken Ceasar Salad",
               "Black Bean Burrito",
               "Falafel Wrap"]

var toDoList = ["Take out garbage", "Pay bills", "Cross off finished items"]

print(toDoList.contains("Take out garbage"))

toDoList.reverse()
print(toDoList)

let index = bucketList.firstIndex(of: "Fly hot air ballon to Fiji") ?? 0
print(index)
